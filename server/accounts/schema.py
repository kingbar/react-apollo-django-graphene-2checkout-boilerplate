import graphene
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required

from accounts.models import User

from core.models import Subscription, Plan


class PlanTypeSubscription(DjangoObjectType):
    class Meta:
        model = Plan


class SubscriptionType(DjangoObjectType):
    plan = graphene.Field(PlanTypeSubscription)

    class Meta:
        model = Subscription


class UserType(DjangoObjectType):
    subscription = graphene.Field(SubscriptionType)
    class Meta:
        model = User


class Query(graphene.ObjectType):
    users = graphene.List(UserType)
    me = graphene.Field(UserType)

    def resolve_users(self, info):
        return User.objects.all()

    @login_required
    def resolve_me(self, info):
        return info.context.user
