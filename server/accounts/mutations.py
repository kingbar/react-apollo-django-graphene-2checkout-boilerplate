from django.contrib.auth.forms import AuthenticationForm
from django.core.files.base import ContentFile
from django.shortcuts import get_object_or_404

import base64

import graphene
from serious_django_graphene import FormMutation, ValidationErrors
from server.tasks import reset_password_email

from .forms import (
    SendConfirmationEmailForm, UserForm, SetNewPasswordForm, UserEditForm
)
from .schema import UserType
from graphql_jwt.shortcuts import get_token
from .tokens import account_activation_token
from .models import User
from core.models import Plan, Subscription

import hmac
import hashlib
from datetime import datetime, timedelta
import requests
import json
import calendar


class RegisterMutation(FormMutation):
    class Meta:
        form_class = UserForm

    token = graphene.String()
    user = graphene.Field(lambda: UserType)

    @classmethod
    def perform_mutate(cls, form, info):
        user = form.save()
        token = get_token(user)
        return cls(
            error=ValidationErrors(validation_errors=[]),
            user=user, token=token, success=True
        )


class LoginMutation(FormMutation):
    class Meta:
        form_class = AuthenticationForm

    token = graphene.String()
    user = graphene.Field(lambda: UserType)

    @classmethod
    def perform_mutate(cls, form, info):
        user = form.get_user()
        token = get_token(user)
        return cls(user=user, token=token)


class UserEditMutation(FormMutation):
    class Meta:
        form_class = UserEditForm

    user = graphene.Field(lambda: UserType)

    @classmethod
    def perform_mutate(cls, form, info):
        user = User.objects.get(email=form.cleaned_data['email'])
        if form.cleaned_data['avatar']:
            img_format, img_str = form.cleaned_data.pop(
                'avatar'
            ).split(';base64,')
            ext = img_format.split('/')[-1]
            avatar = ContentFile(
                base64.b64decode(img_str), name=str(user.id) + ext
            )
            user.avatar = avatar
        else:
            form.cleaned_data.pop('avatar')

        for key, value in form.cleaned_data.items():
            setattr(user, key, value)
        user.save()
        return cls(user=user, error=ValidationErrors(validation_errors=[]),)


class SendConfirmationEmailMutation(FormMutation):
    class Meta:
        form_class = SendConfirmationEmailForm

    email = graphene.String()

    @classmethod
    def perform_mutate(cls, form, info):
        email = form.cleaned_data['email']
        reset_password_email(email)
        return cls(
            success=True
        )


class ResetPasswordMutation(FormMutation):
    class Meta:
        form_class = SetNewPasswordForm

    email = graphene.String()

    @classmethod
    def perform_mutate(cls, form, info):
        confirm_token = form.cleaned_data['confirm_token']
        user = User.objects.get(id=form.cleaned_data['user_id'])

        if account_activation_token.check_token(user, confirm_token):
            user.set_password(form.cleaned_data.get('new_password2'))
            return cls(
                error=ValidationErrors(validation_errors=[]),
                success=True
            )
        else:
            return cls(
                error=ValidationErrors(validation_errors=[]),
                success=False
            )


class CancelSubscriptionMutation(graphene.Mutation):
    class Arguments:
        user_id = graphene.ID()

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    def mutate(root, info, user_id=None):
        success = False
        errors = []
        if not user_id:
            errors.append('User id has not been provided!')
            return CancelSubscriptionMutation(success=success, errors=errors)

        user = get_object_or_404(User, pk=user_id)
        subscription_ref = user.subscription.ref
        # Disable subscription from 2checkout via API
        DATE = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        VENDOR_CODE = "250273992861"
        string_to_hash = str(len(VENDOR_CODE)) + VENDOR_CODE + str(len(DATE)) + DATE
        h = hmac.new(b'&I8G47[^Aq!Jey5VB1lM', bytes(string_to_hash, encoding='utf-8'), digestmod=hashlib.md5).hexdigest()
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Avangate-Authentication": f"code=\"{VENDOR_CODE}\" date=\"{DATE}\" hash=\"{h}\""
        }

        # Use proxy if can't connect to 2checkout API
        #proxies = {
        #   'https': 'https://52.142.122.97:3128',
        #}

        response = requests.delete(
            f"https://api.2checkout.com/rest/6.0/subscriptions/{subscription_ref}/", 
            headers=headers,
        )# add proxies=proxies inside

        if response.text == "true" or 'Cannot disable already disabled subscription' in response.text:
            user.subscription = None
            user.save()
            success = True
        else:
            errors.append('Enable to connect to 2checkout API. Try later!')
        return CancelSubscriptionMutation(success=success, errors=errors)


class CreateSubscriptionMutation(graphene.Mutation):
    class Arguments:
        user_id = graphene.ID()
        plan_id = graphene.ID()
        card_number = graphene.String()
        name = graphene.String()
        email = graphene.String()
        exp_date = graphene.String()
    
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    def mutate(root, info, user_id, plan_id, card_number, name, email, exp_date):
        first_name, last_name = name.split(" ")
        errors = []
        success = False
        user = get_object_or_404(User, pk=user_id)
        plan = get_object_or_404(Plan, pk=plan_id)
        card_exp_year, card_exp_month = exp_date.split('-')

        # Calculate Exp data of subscription
        start_date = datetime.now()
        days_in_month = calendar.monthrange(start_date.year, start_date.month)[1]
        subscription_exp_data = start_date + timedelta(days=days_in_month)
        subscription_exp_data = subscription_exp_data.strftime("%Y-%m-%d")

        # Data to sent to 2checkout API
        data = {
            "AdditionalInfo": None,
            "CustomPriceBillingCyclesLeft": 5,
            "DeliveryInfo": {
                "Codes": [
                {
                    "Code": "___TEST___CODE____",
                    "Description": None,
                    "ExtraInfo": None,
                    "File": None
                }
                ],
                "Description": None
            },
            "EndUser": {
                "Address1": "Test Address",
                "Address2": "",
                "City": "LA",
                "Company": "",
                "CountryCode": "us",
                "Email": email,
                "Fax": "",
                "FirstName": first_name,
                "Language": "en",
                "LastName": last_name,
                "Phone": "",
                "State": "CA",
                "Zip": "12345"
            },
            "ExpirationDate": subscription_exp_data,
            "ExternalCustomerReference": None,
            "ExternalSubscriptionReference": str(user_id),
            "NextRenewalPrice": plan.price,
            "NextRenewalPriceCurrency": "usd",
            "PartnerCode": "",
            "Payment": {
                "CCID": "",
                "CardNumber": card_number,
                "CardType": "MasterCard",
                "ExpirationMonth": card_exp_month,
                "ExpirationYear": card_exp_year,
                "HolderName": name
            },
            "Product": {
                "ProductCode": "test2",
                "ProductId": plan.product_id,
                "ProductName": plan.name,
                "ProductQuantity": 1,
                "ProductVersion": ""
            },
            "StartDate": datetime.now().strftime('%Y-%m-%d'),
            "SubscriptionValue": plan.price,
            "SubscriptionValueCurrency": "usd",
            "Test": 1
            }

        data = json.dumps(data)

        # Generate right headers
        DATE = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        VENDOR_CODE = "250273992861"
        string_to_hash = str(len(VENDOR_CODE)) + VENDOR_CODE + str(len(DATE)) + DATE
        h = hmac.new(b'&I8G47[^Aq!Jey5VB1lM', bytes(string_to_hash, encoding='utf-8'), digestmod=hashlib.md5).hexdigest()
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Avangate-Authentication": f"code=\"{VENDOR_CODE}\" date=\"{DATE}\" hash=\"{h}\""
        }

        # Use proxy if can't connect to 2checkout API
        #proxies = {
        #   'https': 'https://52.142.122.97:3128',
        #}

        # Send request to create a subscribe with provided data
        # Add proxiex-proxies inside
        response = requests.post(f"https://api.2checkout.com/rest/6.0/subscriptions/", headers=headers, data=data)

        # If good requests - create subscription and connect it to user 
        if response.status_code == 201:
            subscription_ref = response.text.replace('"', '')
            subscription = Subscription.objects.create(plan=plan, ref=subscription_ref)
            user.subscription = subscription
            user.save()
            success = True 
        else:
            errors.append('Bad request to 2checkout API')

        return CreateSubscriptionMutation(success=success, errors=errors)
