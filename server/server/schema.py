import graphene
import graphql_jwt
from accounts.mutations import (
    LoginMutation,
    RegisterMutation,
    SendConfirmationEmailMutation,
    ResetPasswordMutation,
    UserEditMutation,
    CancelSubscriptionMutation,
    CreateSubscriptionMutation
)
from accounts.schema import Query as AccountsQuery
from core.schema import Query as CoreQuery


class Query(AccountsQuery, CoreQuery, graphene.ObjectType):
    pass


class Mutation(graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    register = RegisterMutation.Field()
    login = LoginMutation.Field()
    edit_user = UserEditMutation.Field()
    confirm_email = SendConfirmationEmailMutation.Field()
    reset_password = ResetPasswordMutation.Field()
    cancel_subscription = CancelSubscriptionMutation.Field()
    create_subscription = CreateSubscriptionMutation.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)
