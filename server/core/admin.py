from django.contrib import admin
from core.models import Plan, PlanItem, Subscription

admin.site.register(Plan)
admin.site.register(PlanItem)
admin.site.register(Subscription)