import graphene
from graphene_django.types import DjangoObjectType
from django.shortcuts import get_object_or_404
from .utils import get_paginator
from core.models import Plan, PlanItem


class ItemType(DjangoObjectType):
    class Meta:
        model = PlanItem


class PlanType(DjangoObjectType):
    class Meta:
        model = Plan


class Query:
    plans = graphene.List(PlanType)
    items = graphene.List(ItemType, plan_id=graphene.Int())

    def resolve_plans(self, info):
        return Plan.objects.all()
    
    def resolve_items(self, info, plan_id):
        plan = get_object_or_404(Plan, pk=plan_id)
        return PlanItem.objects.filter(plan=plan)
