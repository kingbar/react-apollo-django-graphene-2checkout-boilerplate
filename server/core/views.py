from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404

from core.models import Plan, Subscription

import hmac
import hashlib
from datetime import datetime


@csrf_exempt
def create_order(request): 
    """ Create subscription locally if it was successfully paid """
    # Accept IPN connection
    if request.method == "GET":
        return HttpResponse("Connected")

    # Retrieve data from POST-body
    EXTERNAL_CUSTOMER_REFERENCE = request.POST.get('EXTERNAL_CUSTOMER_REFERENCE')
    IPN_PCODE = request.POST.get('IPN_PCODE[]')
    IPN_LICENSE_REF = request.POST.get('IPN_LICENSE_REF[]')
    IPN_LICENSE_PROD = request.POST.get('IPN_LICENSE_PROD[]')
    IPN_PID = request.POST.get('IPN_PID[]')
    IPN_PNAME = request.POST.get('IPN_PNAME[]')
    IPN_DATE = request.POST.get('IPN_DATE')

    # Creating subscription locally
    if not Subscription.objects.filter(ref=IPN_LICENSE_REF).exists():
        plan = Plan.objects.filter(product_id=IPN_LICENSE_PROD)
        user = get_user_model().objects.filter(pk=EXTERNAL_CUSTOMER_REFERENCE)
        if plan and user:
            user = user.first()
            plan = plan.first()
            subscription = Subscription.objects.create(plan=plan, ref=IPN_LICENSE_REF)
            user.subscription = subscription
            user.save()
            
    DATE = datetime.now().strftime("%Y%m%d%H%M%S")
    msg_response = str(len(IPN_PID)) + IPN_PID + str(len(IPN_PNAME)) + IPN_PNAME + str(len(IPN_DATE)) + IPN_DATE + str(len(DATE)) + DATE
    hash_resp = hmac.new(b'&I8G47[^Aq!Jey5VB1lM', bytes(msg_response, encoding='utf-8'), digestmod=hashlib.md5).hexdigest()
    return HttpResponse(f"<EPAYMENT>{DATE}|{hash_resp}</EPAYMENT>")
        

@csrf_exempt
def license_endpoint(request):
    """ Endpoint to handle Licence changes """
    # Accept LCN Connection
    if request.method == 'GET':
        return HttpResponse("Connected")

    # Retrieve data from POST-body
    LICENSE_CODE = request.POST.get('LICENSE_CODE')
    EXPIRATION_DATE = request.POST.get('EXPIRATION_DATE')
    DATE = datetime.now().strftime("%Y%m%d%H%M%S")

    # Delete subscription locally if it was canceled
    if request.POST.get('STATUS') == 'CANCELED':
        if Subscription.objects.filter(ref=LICENSE_CODE).exists():
            subscription = Subscription.objects.get(ref=LICENSE_CODE)
            subscription.delete()

    # Generate response to LCN listener
    msg_response = str(len(LICENSE_CODE)) + LICENSE_CODE + str(len(EXPIRATION_DATE)) + EXPIRATION_DATE + str(len(DATE)) + DATE 
    hash_response = hmac.new(b'&I8G47[^Aq!Jey5VB1lM', bytes(msg_response, encoding='utf-8'), digestmod=hashlib.md5).hexdigest()
    return HttpResponse(f"<EPAYMENT>{DATE}|{hash_response}</EPAYMENT>")