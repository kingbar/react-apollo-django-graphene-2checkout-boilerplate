# Write your tests for graphql mutations here
from datetime import datetime

import pytest
from graphene.test import Client

from server.schema import schema
from accounts.factories import UserFactory