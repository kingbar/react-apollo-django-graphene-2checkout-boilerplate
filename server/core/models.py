from django.db import models
from django.contrib.auth import get_user_model
from djmoney.models.fields import MoneyField


class Plan(models.Model):
    name = models.CharField(max_length=255)
    price = models.FloatField()
    link = models.URLField(max_length=200)
    code = models.CharField(max_length=255)
    product_id = models.CharField(max_length=25)
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Plan'
        verbose_name_plural = 'Plans'

    def get_items(self):
        return self.items.all()


class PlanItem(models.Model):
    plan = models.ForeignKey("core.Plan", on_delete=models.CASCADE, related_name='items')
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Plan item'
        verbose_name_plural = 'Plan items'


class Subscription(models.Model):
    plan = models.ForeignKey("core.Plan", on_delete=models.CASCADE)
    ref = models.CharField(max_length=50)

    def __str__(self):
        return self.ref

    class Meta:
        verbose_name = 'Subscription'
        verbose_name_plural = 'Subscriptions'
