export const BACKEND_URL = 'http://localhost:8000'
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const TOKEN = "token";
export const MIN_PASSWORD_LENGTH = 8;
export const SIGNATURE_SECRET_WORD = 'Nk9DhmJVT2Q4etug2JNXp%8%59scv$jh7pNvV3@8Qrm&wCt379hqsK?fp9X*TqjG'
