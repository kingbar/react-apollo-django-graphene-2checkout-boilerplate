import React from 'react'

const Checkout = (props) => {
    return (
        <div className="checkout">
        <div className="checkout-container">
            <h3 className="heading-3 pb-0" >Credit card checkout</h3>
            <h5><strong>Subscription to:</strong> {props.plan ? props.plan.name : 'Plan'}</h5>
            <h5><strong>Price:</strong> {props.plan ? props.plan.price : 'Price'} $</h5>
            <Input label="Cardholder's Name" type="text" name="name" />
            <Input label="Card Number" type="number" name="card_number" imgSrc="https://seeklogo.com/images/V/visa-logo-6F4057663D-seeklogo.com.png" />
            <div className="row">
            <div className="col">
                <Input label="Expiration Date" type="month" name="exp_date" />
            </div>
            <div className="col">
                <Input label="CVV"  type="number" name="cvv" />
            </div>
            </div>
            <Input label='Email' type='email' name='email' />
            <Button text="Place order" submitForm={props.submitForm} plan_id={props.plan ? props.plan.id : -1}/>
        </div>
        </div>
    )};

   const Input = (props) => (
    <div className="input">
      <label>{props.label}</label>
      <div className="input-field">
        <input type={props.type} name={props.name} />
        <img src={props.imgSrc}/>
      </div>
    </div>
  );


const Button = (props) => (
    <button className="checkout-btn" onClick={() => props.submitForm(props.plan_id)} type="button">{props.text}</button>
  );


export default Checkout;