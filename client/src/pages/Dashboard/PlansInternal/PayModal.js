import React from "react"
import Modal from "@material-ui/core/Modal"
import Card from "@material-ui/core/Card"
import Checkout from "./Checkout"

const PayModal = (props) => {

    return (
        <Modal
            aria-labelledby="transition-modal-title-vcenter"
            aria-describedby="transition-modal-description"
            open={props.isOpen}
            onClose={props.closeModal}
            style={{ backdropFilter: "blur(5px)" }}
            closeAfterTransition
            >
            <Card
                style={{
                maxWidth: "650px",
                minWidth: "350px",
                width:"100%",
                transition: "0.5s",
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)"
                }}
            >
                <div className="app-container" style={{margin: 0}}>
                    <div className="row">
                        <div className="col no-gutters" id="cardForm" style={{padding: 0}}>
                            <Checkout plan={props.plan} submitForm={props.submitForm}/>
                        </div>
                    </div>
                </div>
            </Card>
        </Modal>
    )

}

export default PayModal;