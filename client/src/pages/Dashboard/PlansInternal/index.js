import React, { useState, useEffect } from "react";
import { graphql, Query, useQuery, useMutation } from "react-apollo";
import _ from "lodash";
import Profile from "../../../components/UserProfile";
import UserEditForm from "../../../components/Forms/UserEditForm";
import {MDBCard, MDBBtn} from "mdbreact"
import i18n from "../../../i18n";

import { getBase64 } from "../../../utils";
import { User, editUser, getPlans, cancelSubscription,  createSubscription} from "../../../queries";
import ScriptTag from "react-script-tag";
import sha256 from "js-sha256"
import { SIGNATURE_SECRET_WORD } from "../../../constants/index";
import PayModal from "./PayModal"


const PlansInternal = ({ edit, user }) => {
  const [cancelSub, {cancelSubData} ] = useMutation(cancelSubscription);
  const [createSub, {createSubData} ] = useMutation(createSubscription);
  const [a, setA] = useState(false);
  const [isOpen, setOpen] = useState(false);
  const [selectedPlan, setSelectedPlan] = useState(null);

  const stylesOnCard = { width: "22rem", marginTop: "1rem", color: "#5f7ad4"};
  const me = 'me' in user ? user.me : -1;
  var my_subscription_plan = -1;
  if (me != -1){
      if(me.subscription){
        my_subscription_plan = me.subscription.plan.id;
      }
      else{
          my_subscription_plan = -1;
      }
  }

  const openModal = (plan) => {
    setOpen(true);
    setSelectedPlan(plan);
  }

  const closeModal = () => {
    setOpen(false);
  }

  const submitForm = async (plan_id) => {
    var cardNumber = document.querySelector('#cardForm input[name=card_number]').value;
    var name = document.querySelector('#cardForm input[name=name]').value;
    var expDate = document.querySelector('#cardForm input[name=exp_date]').value;
    var cvv = document.querySelector('#cardForm input[name=cvv]').value;
    var email = document.querySelector("#cardForm input[name=email]").value;
    document.getElementById(`buySubButton_${plan_id}`).innerHTML = "In processing...";
    closeModal();
    var userId = me.id;
    var result = await createSub({
        variables: {
            'cardNumber': cardNumber,
            'email': email,
            'expDate': expDate,
            'name': name,
            'planId': plan_id,
            'userId': userId
        },
        refetchQueries: [
            {
                query: User
            }
        ]
    })
    if (result.data.createSubscription.result == true){
        setA(!a);
    }
  }

  const handleCancelSubscription = async (plan_id,refetch) => {
    document.getElementById(`cancelSubButton_${plan_id}`).innerHTML = "In processing...";
    var userId = me.id;
    var result = await cancelSub({
        variables: {
            'userId': userId
        },
        refetchQueries: [{
            query: User,
        }]
    })  
    if (result.data.cancelSubscription.success == true){
        document.getElementById(`cancelSubButton_${plan_id}`).innerHTML = "BUY NOW";
        setA(!a);
    }
  }
  
  return (
      <div id='plansContent' style={{display: "flex", flexWrap: "wrap"}}>
            <Query query={getPlans}>
            {({ loading, error, data, refetch }) => {
            if (loading) return null;
            if (error) return `Error! ${error.message}`;
            return (
                data.plans.map((plan) => {
                    return (
                        <MDBCard key={plan.pk} className="card-body mx-2" style={stylesOnCard}>
                        <h3 className='text-center' style={{fontWeight: "600"}}>
                            {plan.name}
                        </h3>
                        <h4 className='mt-3 text-center' style={{fontWeight: "600"}}>{plan.price} $</h4>
                        <ul className="mt-3" style={{color: "#444", height:"100px", listStyle:"none", marginLeft: "0px", textAlign:"center", paddingLeft: "0px"}}>
                            {plan.items.map((item) => {
                                return (
                                    <li key={item.pk}>{item.name}</li>
                                )
                            })}
                        </ul>
                        <hr />
                        {my_subscription_plan == plan.id ? (
                            <MDBBtn
                            color="secondary"
                            style={{maxWidth: "200px"}}
                            className="mx-auto"
                            key={`cancelSubButton_${plan.id}`}
                            id={`cancelSubButton_${plan.id}`}
                            onClick={() => handleCancelSubscription(plan.id,refetch)}>
                                CANCEL
                            </MDBBtn>
                        ) :
                        (                     
                            <MDBBtn
                            color="primary"
                            style={{maxWidth: "200px"}}
                            className="mx-auto"
                            key={`createSubButton_${plan.id}`}
                            id={`buySubButton_${plan.id}`}
                            onClick={() => openModal(plan)}>
                                BUY NOW 
                            </MDBBtn>)
                        }
                        </MDBCard>
                    );
                })
            );
            }}
        </Query>

        <PayModal 
            plan={selectedPlan} 
            isOpen={isOpen} 
            closeModal={closeModal}
            submitForm={submitForm}
        />
    </div>
  );
};

export default _.flowRight(
  graphql(editUser, { name: "edit" }),
  graphql(User, { name: "user" })
)(PlansInternal);
