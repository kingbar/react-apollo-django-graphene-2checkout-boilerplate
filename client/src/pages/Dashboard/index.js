import React, { Fragment } from "react";

import GetUsers from "../../components/Statistics/GetUsers";

const Main = () => (
  <Fragment>
    <GetUsers />
  </Fragment>
);

export default Main;
