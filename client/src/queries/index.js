import gql from "graphql-tag";

export const register = gql `
  mutation register(
    $email: String!
    $password1: String!
    $password2: String!
    $fullName: String!
  ) {
    register(
      email: $email
      password1: $password1
      password2: $password2
      fullName: $fullName
    ) {
      error {
        __typename
        ... on ValidationErrors {
          validationErrors {
            field
            messages
          }
        }
      }
      success
      token
      user {
        id
        email
        fullName
      }
    }
  }
`;

export const login = gql `
  mutation login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      error {
        __typename
        ... on ValidationErrors {
          validationErrors {
            field
            messages
          }
        }
      }
      token
      user {
        id
        email
        fullName
      }
    }
  }
`;

export const editUser = gql `
  mutation editUser(
    $fullName: String
    $email: String!
    $avatar: String
  ) {
    editUser(fullName: $fullName, email: $email, avatar: $avatar) {
      error {
        __typename
        ... on ValidationErrors {
          validationErrors {
            field
            messages
          }
        }
      }
      user {
        id
        fullName
        email
        avatar
      }
    }
  }
`;

export const verifyToken = gql `
  mutation verifyToken($token: String!) {
    verifyToken(token: $token) {
      payload
    }
  }
`;

export const getUsers = gql `
  query getUsers {
    users {
      id
      fullName
      email
    }
  }
`;

export const User = gql `
  query me {
    me {
      id
      email
      fullName
      subscription{
        id
        plan {
          id 
          name
        }
      }
    }
  }
`;

export const confirmEmail = gql `
  mutation confirmEmail($email: String!) {
    confirmEmail(email:$email){
      success
      error {
        __typename
        ... on ValidationErrors {
          validationErrors {
            field
            messages
          }
        }
      }
    }
  }
`;

export const resetPassword = gql `
  mutation resetPassword($newPassword1: String!, $newPassword2: String!, $confirmToken: String!, $userId: Int!) {
    resetPassword(newPassword1: $newPassword1, newPassword2: $newPassword2, confirmToken: $confirmToken, userId: $userId){
      success
      error {
        __typename
        ... on ValidationErrors {
          validationErrors {
            field
            messages
          }
        }
      }
    }
  }
`;

export const getItems = gql `
  query getItems(
    $planId: ID!
  ) {
    items(planId: $planId){
      id 
      name
      
    }
  }
`;

export const getPlans = gql `
  query getPlans {
    plans {
      id
      name
      code
      productId
      price
      link
      items{
        id
        name
      }
    }
  }
`;

export const cancelSubscription = gql `
  mutation cancelSubscription(
    $userId: ID!
  ) {
    cancelSubscription(userId: $userId)
    {
      success
      errors
    }
  }
`;

export const createSubscription = gql `
  mutation createSubscription(
    $cardNumber: String
    $email: String
    $expDate: String
    $name: String
    $planId: ID
    $userId: ID
  ) {
    createSubscription(
      cardNumber: $cardNumber,
      email: $email,
      expDate: $expDate,
      name: $name,
      planId: $planId,
      userId: $userId
    )
    {
      success
      errors
    }
  }
`;